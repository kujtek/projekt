#include <iostream>
#include <conio.h>
#include <Windows.h>
#include <math.h>

using namespace std;

void gotoxy(int, int);
void slash(int, int, int, char, int&);
void backslash(int, int, int, char, int&);
void draw_figure(int&, int&, int&, char&, char&, char&, int&, int&, int&, int, int);
void move(char, int&, int&, int&, int, int, int, int);
void menu();
void get_console_size(int&, int&);


void main() {

	int console_height, console_width;
	int x, y, bok, xx, yy, exit_key;
	char key, ascii_char, choose;

	get_console_size(console_height, console_width);
	draw_figure(x, y, bok, key, ascii_char, choose, exit_key, xx, yy, console_height, console_width);
}


void menu() {
	{
		cout << "witaj! \nwcisnij enter by zaczac...\n";
	}
}

void get_console_size(int& console_height, int& console_width)
{
	CONSOLE_SCREEN_BUFFER_INFO csbi; // pobieramy informacje o rozmiarze okna uzywajac winapi
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	console_width = csbi.srWindow.Right - csbi.srWindow.Left + 1; // szerokosc
	console_height = csbi.srWindow.Bottom - csbi.srWindow.Top + 1; // wysokosc
}

void draw_figure(int& x, int& y, int& bok, char& key, char& ascii_char, char& choose, int& exit_key, int& xx, int& yy, int console_height, int console_width)
{
	while (choose != 27)
	{
		x = 0;
		y = 0;
		system("cls");
		menu();
		choose = _getch();
		system( "cls" );

		switch( choose )
		{
		case 13: {
			while ( ( bok < 4 ) || ( bok > 24 ) ) {
				cout << "Podaj wysokosc figury w przedziale od 6 do 24 ";
				cin >> bok;
				cout << "\n";
			}

			while ( ( ascii_char < 0 ) || ( ascii_char > 126 ) ) {
				cout << "Podaj ascii_char figury ";
				cin >> ascii_char;
			}

			while ( key != 27)
			{
				system("cls");
				backslash(x, y, bok, ascii_char, yy);
				slash(x, y, bok, ascii_char, xx);
				key = _getch();
				move(key, bok, x, y, xx, yy, console_height, console_width);
			}
		}
		}
	}
}


void move(char key, int &bok, int &x, int &y, int xx, int yy, int console_height, int console_width) {

	if ( ( key == '-' ) && ( bok > 4 ) )
	{
		bok = bok - 1; // co dwa ascii_chari żeby ładnie wyglądał nasz iks
	}
	if ( ( key == '+' ) && ( ( xx ) < console_width) && ( ( yy ) < console_height ) )
	{
		bok = bok + 1; // co dwa ascii_chari żeby ładnie wyglądał nasz iks
	}
	if ( ( key == 75 ) && ( x > 0 ) ) // w lewo
	{
		x--;
	}
	if  ( ( key == 77 ) && ( (xx) < console_width ) ) // w prwao
	{
		x++;
	}
	if  ( ( key == 72 ) && y > 0) // w górę
	{
		y--;
	}
	if  ( ( key == 80 ) && ( (yy) < console_height ) ) // w dół
	{
		y++;
	}

}


void gotoxy(int x, int y)
{
	/*
	gotoxy jest funkcją z pascala, a pascal jest zły!
	trzeba trochę to naciągnąć wstawką z winapi :D
	*/
	static HANDLE h = NULL;
	if (!h)
		h = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD c = { x, y };
	SetConsoleCursorPosition(h, c);
}

// linia lewo-dół prawo-góra
void slash(int x, int y, int bok, char ascii_char, int &xx)
{
	if (bok % 2 != 0)
	{
		for (int i = 1; i <= bok ; i++)
		{

			gotoxy(x, y + bok - 1);
			cout << ascii_char;
			//cout << "\n" << bok;
			//cout << "\n" << y;
			x++; y--;
		}
	}
	else
	{
		for (int i = 1; i <= bok; i++)
		{

			gotoxy(x, y + bok - 1);
			cout << ascii_char;
			//cout << "\n x = " << bok;
			//cout << "\n y = " << y;

			x++; y--;
		}
	}
	xx = x;
}

// linia "lewo-góra prawo-dół
void backslash(int x, int y, int bok, char ascii_char, int &yy)
{
	if (bok % 2 != 0)

		for (int i = 1; i <= bok ; i++)
		{
			gotoxy(x, y);
			cout << ascii_char;
			x++; y++;
		}
	else
	{
		for (int i = 1; i <= bok ; i++)
		{
			gotoxy(x, y);
			cout << ascii_char;
			x++; y++;
		}
	}
	yy = y;
}
